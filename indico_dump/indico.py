"""Main module for indico_dump."""

from datetime import datetime
import hashlib
import hmac
import json
import sys
import time
import urllib.error
import warnings

import numpy as np
import pandas as pd
import requests

from indico_dump.logger import logger
from indico_dump.utils import BearerAuth


class Indico:
    """Main class to connect to indico server."""

    def __init__(
        self,
        indico_site: str = "https://indico.cern.ch",
        api_token: str = None,
        secret_key: str = None,
        api_key: str = None,
    ) -> None:
        """Initialise Indico class.

        Parameters
        ----------
        indico_site : str, optional
            indico website, by default "https://indico.cern.ch"
        api_token : str, optional
            API token available from indico version >=3, by default None
        secret_key : str, optional
            secret key (deprecetad in indico version >=3), by default None
        api_key : str, optional
            api key (deprecetad in indico version >=3), by default None
        """
        logger.debug("Initialise Indico connection.")
        # setup of indico connection
        self.indico_site = indico_site
        self.api_token = api_token
        self.only_public = False
        # these two options are outdated, but still used for institute indico servers
        # which have versions < 3.
        self.secret_key = secret_key
        self.api_key = api_key
        self.deprecated_api = False
        self.persistent = False

        self._check_api_conformity()

    def _check_api_conformity(self):
        """Check old and new HTTP APIs conformity.

        Raises
        ------
        ValueError
            if `secret_key` is specified but not `api_key`
        ValueError
            if old and new API are mixed
        """
        if self.secret_key is not None or self.api_key is not None:
            if self.secret_key is not None and self.api_key is None:
                raise ValueError(
                    "When specifying `secret_key` you need to also specify `api_key`."
                )
            if self.api_token is not None:
                raise ValueError(
                    "`api_token` and `secret_key`/`api_key` are not compatible. "
                    "Specify only one api group."
                )
            warnings.warn(
                "`api_key` and `secret_key` are deprecated with indico version >3. "
                "Please use `api_tokens`.",
                DeprecationWarning,
                stacklevel=2,
            )
            self.deprecated_api = True

    def category(self, category_id: int, start_date: str, end_date: str = "+21d"):
        """Initialise category.

        Parameters
        ----------
        category_id : int
            category ID
        start_date : str
            start date
        end_date : str, optional
            end date, by default "+14d"

        Returns
        -------
        Category
            Category class object
        """
        categ = Category(indico=self)
        categ.get(category_id=category_id, start_date=start_date, end_date=end_date)
        return categ

    def request(self, path: str, params: dict):
        """Perform indico request.

        Parameters
        ----------
        path : str
            prepared path e.g. `/export/categ/%d.json`
        params : dict
            parameters added to url

        Returns
        -------
        dict
            request
        """
        items = list(params.items()) if hasattr(params, "items") else list(params)
        if self.only_public:
            items.append(("onlypublic", "yes"))
        if self.deprecated_api:
            if self.api_key:
                items.append(("apikey", self.api_key))
            if self.secret_key:
                if not self.persistent:
                    items.append(("timestamp", str(int(time.time()))))
                items = sorted(items, key=lambda x: x[0].lower())
                sig_url = f"{path}?{urllib.parse.urlencode(items)}"
                signature = hmac.new(
                    self.secret_key.encode("utf-8"),
                    sig_url.encode("utf-8"),
                    hashlib.sha1,
                ).hexdigest()
                items.append(("signature", signature))
            url = f"{self.indico_site}{path}?{urllib.parse.urlencode(items)}"
            logger.debug(url)
            try:
                req = urllib.request.Request(url)
                return json.loads(urllib.request.urlopen(req).read().decode("utf-8"))
            except urllib.error.HTTPError as error:
                print(f"An HTTPError occurred, exiting: {str(error)}")
                sys.exit(1)
        else:
            items = sorted(items, key=lambda x: x[0].lower())
            url = f"{self.indico_site}{path}?{urllib.parse.urlencode(items)}"
            logger.debug(url)
            response = requests.get(url, auth=BearerAuth(self.api_token))
            response.encoding = response.apparent_encoding
            return response.json()


class IndicoObject:  # pylint: disable=R0903
    """Base object for indico info."""

    def __init__(self, indico, name: str = "") -> None:
        """Initialise class.

        Parameters
        ----------
        indico : Indico class
            Indico class object
        name : str, optional
            Name of object, by default ""
        """
        self.indico = indico
        self.name = name
        self.meta_data = None
        self.url = None

    def to_dict(self):
        """Converts class attributes to dict.

        Returns
        -------
        dict
            attribute summary as a dictionary
        """
        attribute_summary = {}
        exclude = (
            "indico",
            "name",
            "meta_data",
            "event",
            "n_iter",
            "detail",
            "session_iter",
            "datetime",
        )
        for attribute, value in self.__dict__.items():
            if attribute in exclude:
                continue
            attribute_summary[attribute] = value
        return attribute_summary


class Category(IndicoObject):  # pylint: disable=R0902
    """Indico category object."""

    def __init__(self, *args, **kwargs) -> None:
        """Initialise class.

        Parameters
        ----------
        *args : args
            args for IndicoObject
        **kwargs : kwargs
            kwargs for IndicoObject
        """
        super().__init__(*args, **kwargs)
        self.category_id = None
        self.start_data = None
        self.end_data = None
        self.events_df = None
        self.count = None
        self.filtered_df = None
        self.iter = None
        self.n_iter = None
        self.n_iter_max = None

    def get(self, category_id: int, start_date: str, end_date: str):
        """Retrieve cetegory from indico server.

        Parameters
        ----------
        category_id : int
            category id
        start_date : str
            start date
        end_date : str
            end date
        """
        category_params = {
            "from": start_date,
            "to": end_date,
            "pretty": "yes",
            "order": "start",
        }
        category_path = f"/export/categ/{category_id}.json"
        self.meta_data = self.indico.request(category_path, params=category_params)
        self._initialise()

    def _initialise(self):
        """Initialise category."""
        self.count = self.meta_data["count"]
        self.url = self.meta_data["additionalInfo"]["eventCategories"][-1]["path"][-2][
            "url"
        ]
        logger.debug("Reset `event_titles` and `event_dates`.")
        event_list = []

        for event in reversed(self.meta_data["results"]):
            event_list.append(
                {
                    "title": event["title"],
                    "date": event["startDate"]["date"],
                    "id": event["id"],
                }
            )
        self.events_df = pd.DataFrame(event_list)
        self.events_df["date"] = pd.to_datetime(self.events_df["date"])

    def filter_events(
        self,
        start_date: str = None,
        end_date: str = None,
        include_list: list = None,
        exclude_list: list = None,
    ):
        """Filter events in category based on arguments.

        Parameters
        ----------
        start_date : str, optional
            start date, by default None
        end_date : str, optional
            end date, by default None
        include_list : list, optional
            list with strings which should be in title - checked as OR operation,
            by default None
        exclude_list : list, optional
            list with strings which should not be in title - checked as OR operation,
            by default None

        Returns
        -------
        pd.DataFrame
            DataFrame with filtered events

        Raises
        ------
        ValueError
            if category not yet initialised
        """
        start_date = pd.to_datetime(start_date)
        if self.events_df is None:
            raise ValueError("You first need to initialise a category with `get()`.")
        date_query = f"date>= '{start_date}'" if start_date is not None else ""
        date_query += f"date<= '{end_date}'" if end_date is not None else ""
        df_select = (
            self.events_df.query(date_query) if date_query != "" else self.events_df
        )

        includes = (
            df_select.title.apply(
                lambda title: any(
                    patt.lower() in title.lower() for patt in include_list
                )
            ).values
            if include_list
            else [True] * len(df_select)
        )
        excludes = (
            df_select.title.apply(
                lambda title: any(
                    patt.lower() not in title.lower() for patt in exclude_list
                )
            ).values
            if exclude_list
            else [True] * len(df_select)
        )

        # using here numpy `logical_and` though could also use a list comprehension
        # if numpy dependence is not wanted
        mask = np.logical_and(includes, excludes)
        self.filtered_df = df_select[mask]
        return df_select[mask]

    def event(self, event_id: int):
        """Retrieve event.

        Parameters
        ----------
        event_id : int
            ID of the indico event

        Returns
        -------
        Event
            Event class object
        """
        indico_event = Event(indico=self.indico)
        indico_event.get(event_id)
        return indico_event

    def __iter__(self):
        """Class iterator.

        Returns
        -------
        self
            class object itself

        Raises
        ------
        ValueError
            if event not initialised.
        """
        if self.events_df is None:
            raise ValueError(
                "Before iterating you need to initialise an event via `get()`."
            )
        if self.filtered_df is None:
            logger.warning(
                "Iterating over full category without filters. This could take a while."
            )
            self.iter = self.events_df["id"].values
        else:
            self.iter = self.filtered_df["id"].values  # pylint: disable=E1136
        self.n_iter_max = len(self.iter)
        self.n_iter = 0
        return self

    def __next__(self):
        """Next class operator.

        Returns
        -------
        Event
            Event class object

        Raises
        ------
        StopIteration
            when end of iteration is reached
        """
        if self.n_iter < self.n_iter_max:
            event = Event(self.indico)
            event_id = self.iter[self.n_iter]
            event.get(event_id)
            self.n_iter += 1
            return event

        raise StopIteration

    def reset_filer(self):
        """Reset event filter for the category."""
        self.filtered_df = None

    def save_events(self, file_name: str):
        """Save events to json file.

        Parameters
        ----------
        file_name : str
            Name of output file.
        """
        event_summary = []
        for event in self:
            logger.debug("------")
            logger.debug(f"Event title: {event.title}")
            logger.debug(f"Event date: {event.date}")
            logger.debug(f"Event id: {event.event_id}")
            event.initlialise_contributions()
            event_summary.append(event.to_dict())

        logger.info(f"Saving file to `{file_name}`.")
        with open(file_name, "w") as json_file:  # pylint: disable=W1514
            json.dump(event_summary, json_file, indent=4)


class Event(IndicoObject):  # pylint: disable=R0902
    """Indico event object."""

    def __init__(self, *args, **kwargs) -> None:
        """Initialise class.

        Parameters
        ----------
        *args : args
            args for IndicoObject
        **kwargs : kwargs
            kwargs for IndicoObject
        """
        super().__init__(*args, **kwargs)
        self.event_id = None
        self.title = None
        self.date = None
        self.time = None
        self.minutes_url = None
        self.minutes = None
        self.room = None
        self.room_fullname = None
        self.location = None
        self.contributions = None
        self.n_contributions = None
        self.n_sessions = None
        self.session_map = None
        self.session_iter = None

        self.n_iter = None
        self.detail = None

    def get(self, event_id: int, detail: str = "subcontributions"):
        """Retrieve event from indico server.

        Parameters
        ----------
        event_id : int
            event id
        detail : str, optional
            Detail level requesting the event from indico server. More details under
            https://docs.getindico.io/en/stable/http-api/exporters/event/#parameters,
            for now only `subcontributions` and `sessions` are supported,
            by default "subcontributions"

        Raises
        ------
        ValueError
            if unsupported detail parameter provided
        """
        self.event_id = event_id
        if detail not in ["subcontributions", "sessions"]:
            raise ValueError(
                "Only 'subcontributions' and 'sessions' option supported for event "
                "detail level."
            )
        self.detail = detail
        event_params = {"pretty": "yes", "detail": detail}
        event_path = f"/export/event/{event_id}.json"
        self.meta_data = self.indico.request(event_path, params=event_params)
        self._initialise()

    def _initialise(self):
        """Initialise event."""
        results = self.meta_data["results"][0]
        if "note" in list(results.keys()):
            if "url" in results["note"]:
                self.minutes_url = results["note"]["url"]
            if "html" in results["note"]:
                self.minutes = results["note"]["html"]
        self.url = results["url"]
        self.title = results["title"]
        self.date = results["startDate"]["date"]
        self.time = results["startDate"]["time"]
        self.room = results["room"]
        self.room_fullname = results["roomFullname"]
        self.location = results["location"]
        if self.detail == "subcontributions":
            self.n_contributions = len(results["contributions"])
        else:
            self.n_sessions = len(results["sessions"])
            self.n_contributions = len(results["contributions"])
            self.session_iter = [
                {"contr_id": i, "session_id": None} for i in range(self.n_contributions)
            ]
            self.session_map = {}
            self.session_map["non_session"] = self.n_contributions
            for session_id, session in enumerate(results["sessions"]):
                self.n_contributions += len(session["contributions"])
                self.session_map[session["session"]["title"]] = len(
                    session["contributions"]
                )
                self.session_iter += [
                    {"contr_id": i, "session_id": session_id}
                    for i in range(len(session["contributions"]))
                ]

    def __iter__(self):
        """Class iterator.

        Returns
        -------
        self
            class object itself

        Raises
        ------
        ValueError
            if event not initialised.
        """
        if self.n_contributions is None:
            raise ValueError(
                "Before iterating you need to initialise an event via `get()`."
            )
        if self.detail == "subcontributions":
            self.n_iter = self.n_contributions - 1
        elif self.detail == "sessions":
            self.n_iter = 0
        return self

    def __next__(self):
        """Next class operator.

        Returns
        -------
        Contribution
            Contribution class object

        Raises
        ------
        StopIteration
            when end of iteration is reached
        """
        if self.detail == "subcontributions":
            if self.n_iter >= 0:
                contr = Contribution(self)
                contr.get(self.n_iter)
                self.n_iter -= 1
                return contr
        elif self.detail == "sessions":
            if self.n_iter < self.n_contributions:
                contr = Contribution(self)
                contr.get(**self.session_iter[self.n_iter])
                self.n_iter += 1
                return contr

        raise StopIteration

    def initlialise_contributions(self):
        """Initialise contribitions and stores them as class variable."""
        self.contributions = []
        contr_dates = []
        for contr in self:
            if contr.date is None:
                continue
            self.contributions.append(contr.to_dict())
            contr_dates.append(contr.datetime)
        # sort the contributions by their start time
        self.contributions = [
            x
            for _, x in sorted(
                zip(contr_dates, self.contributions), key=lambda pair: pair[0]
            )
        ]


class Contribution(IndicoObject):  # pylint: disable=R0902
    """Indico contribution object."""

    def __init__(self, event: Event) -> None:
        """Initialise class.

        Parameters
        ----------
        event : Event
            event object
        """
        super().__init__(indico=None)
        self.event = event
        self.title = None
        self.date = None
        self.time = None
        self.datetime = None
        self.duration = None
        self.minutes_url = None
        self.minutes = None
        self.speakers = None
        self.n_attachements = None
        self.session = None
        self.session_id = None
        self.sub_contributions = None
        self.n_sub_contributions = None
        self.contr_id = None
        self.track = None

    def get(self, contr_id: int, session_id: int = None):
        """Extract contribution info.

        Parameters
        ----------
        contr_id : int
            contribution id
        session_id : int
            session id
        """
        self.contr_id = contr_id
        self.session_id = session_id
        self._initialise()

    def _initialise(self):
        """Initialise contribution.

        Returns
        -------
        None
        """
        if self.session_id is None:
            self.meta_data = self.event.meta_data["results"][0]["contributions"][
                self.contr_id
            ]
        else:
            self.meta_data = self.event.meta_data["results"][0]["sessions"][
                self.session_id
            ]["contributions"][self.contr_id]

        if self.meta_data["startDate"] is None:
            self.date = None
            return None
        self.title = self.meta_data["title"]
        self.date = self.meta_data["startDate"]["date"]
        self.time = self.meta_data["startDate"]["time"]
        self.datetime = datetime.strptime(
            f"{self.date} {self.time}", "%Y-%m-%d %H:%M:%S"
        )
        self.duration = self.meta_data["duration"]
        if "note" in list(self.meta_data.keys()):
            if "url" in self.meta_data["note"]:
                self.minutes_url = self.meta_data["note"]["url"]
            if "html" in self.meta_data["note"]:
                self.minutes = self.meta_data["note"]["html"]
        self.speakers = [
            f"{speaker['first_name']} {speaker['last_name']}"
            for speaker in self.meta_data["speakers"]
        ]

        if self.meta_data["folders"]:
            self.n_attachements = len(self.meta_data["folders"][0]["attachments"])
        self.session = self.meta_data["session"]
        self.track = self.meta_data["track"]
        self.url = self.meta_data["url"]

        self.n_sub_contributions = len(self.meta_data["subContributions"])

        if self.meta_data["subContributions"]:
            self.sub_contributions = []
            for sub_contrib in self.meta_data["subContributions"]:
                sub_contrib_i = {}
                sub_contrib_i["title"] = sub_contrib["title"]
                sub_contrib_i[
                    "url"
                ] = f"{self.url}subcontributions/{sub_contrib['db_id']}"

                sub_contrib_i["speakers"] = [
                    f"{speaker['first_name']} {speaker['last_name']}"
                    for speaker in sub_contrib["speakers"]
                ]
                self.sub_contributions.append(sub_contrib_i)
        return None
