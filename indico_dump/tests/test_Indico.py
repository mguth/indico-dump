"""Tests for Indico class."""

import unittest

from indico_dump.indico import Indico


class Indico_TestCase(unittest.TestCase):
    def test_old_api_init(self):

        with self.subTest("api_key only"):
            with self.assertWarns(Warning):
                Indico(api_key="test")
        with self.subTest("api_key and secret_key"):
            with self.assertWarns(Warning):
                Indico(api_key="test", secret_key="test2")
        with self.subTest("only secret_key"):
            with self.assertRaises(ValueError):
                Indico(secret_key="test2")
        with self.subTest("old and new API"):
            with self.assertRaises(ValueError):
                Indico(api_key="test", api_token="test3")
