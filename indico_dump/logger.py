"""Configuration for logger of indico_dump."""
import logging


def get_logger():
    """Initialise logger.

    Returns
    -------
    object
        logger
    """
    indico_dump_logger = logging.getLogger("indico_dump")
    indico_dump_logger.setLevel(logging.INFO)

    logger_ch = logging.StreamHandler()
    logger_ch.setLevel(logging.INFO)
    logger_ch.setFormatter(CustomFormatter())

    indico_dump_logger.addHandler(logger_ch)
    indico_dump_logger.propagate = False
    return indico_dump_logger


class CustomFormatter(logging.Formatter):
    """Logging Formatter to add colors and count warning / errors
    using implementation from
    https://stackoverflow.com/questions/384076/how-can-i-color-python-logging-output"""

    grey = "\x1b[38;21m"
    yellow = "\x1b[33;21m"
    green = "\x1b[32;21m"
    red = "\x1b[31;21m"
    bold_red = "\x1b[31;1m"
    reset = "\x1b[0m"
    debugformat = (
        "%(asctime)s - %(levelname)s:%(name)s: %(message)s (%(filename)s:%(lineno)d)"
    )
    date_format = "%(levelname)s:%(name)s: %(message)s"

    FORMATS = {
        logging.DEBUG: grey + debugformat + reset,
        logging.INFO: green + date_format + reset,
        logging.WARNING: yellow + date_format + reset,
        logging.ERROR: red + debugformat + reset,
        logging.CRITICAL: bold_red + debugformat + reset,
    }

    def format(self, record):
        log_fmt = self.FORMATS.get(record.levelno)
        formatter = logging.Formatter(log_fmt)
        return formatter.format(record)


def set_log_level(indico_dump_logger, log_level: str):
    """Setting log level

    Parameters
    ----------
    indico_dump_logger : logger
        logger object
    log_level : str
        logging level corresponding CRITICAL, ERROR, WARNING, INFO, DEBUG, NOTSET
    """
    log_levels = {
        "CRITICAL": logging.CRITICAL,
        "ERROR": logging.ERROR,
        "WARNING": logging.WARNING,
        "INFO": logging.INFO,
        "DEBUG": logging.DEBUG,
        "NOTSET": logging.NOTSET,
    }
    indico_dump_logger.setLevel(log_levels[log_level])
    for handler in indico_dump_logger.handlers:
        handler.setLevel(log_levels[log_level])


logger = get_logger()
logger.debug("Initialised logger.")
