"""Utils for indico_dump module."""
import requests


class BearerAuth(requests.auth.AuthBase):  # pylint: disable=R0903
    """Bearer authentification class."""

    def __init__(self, token: str):
        """Initialise BearerAuth class.

        Parameters
        ----------
        token : str
            Bearer token
        """
        self.token = token

    def __call__(self, req):
        """Call function.

        Parameters
        ----------
        req : object
            request object

        Returns
        -------
        object
            request
        """
        req.headers["authorization"] = "Bearer " + self.token
        return req
