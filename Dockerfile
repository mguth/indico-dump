FROM ${BASE_IMAGE}

COPY . /indico_dump
RUN cd /indico_dump && python -m pip install .
