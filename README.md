# !!! MOVED TO https://gitlab.com/guth.manuel/indico-dump !!!





# Indico Dump Project

Python wrapper around HTTP API of indico to dump `json` files

## Example


```python
from indico_dump.indico import Indico
import os
from indico_dump.logger import logger, set_log_level

set_log_level(logger, "DEBUG")

# initialise CERN Indico
indico_cern = Indico(api_token=os.environ["INDICO_API"])

category = indico_cern.category(category_id=9120, start_date="2022-01-01")

# FTAG meetings
category.filter_events(include_list=["Algorithms"])
category.save_events("FTAGAlgoMeetings.json")

```
